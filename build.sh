#!/bin/sh

if [ ! -d "venv" ]
then
    echo "Create python virtualenv"
    python3 -m venv venv
    echo "Installing dependencies"
    source venv/bin/activate
    pip install -r requirements.txt
else
    echo "Activating virtualenv"
    source venv/bin/activate
fi

echo "Building documentation"
sphinx-build -t html source/ out/

if [ "$1" = open ]
then
    echo "Opening documentation in default browser"
    xdg-open out/index.html
else
    echo "\n\nIf you want to automatically open the documentation, run './build.sh open'"
fi
